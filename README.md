
## Contácto

Daniel Pérez Cabrera    
Cel: 55 1120 0160    
Email:  creativedesing.dpc@gmail.com

## Descripción

SPA (Single Page Application) que consulta artículos de Mercado libre.    


## Instalación

```bash
$ yarn install
```

## Ejecución

```bash
# Desarrollo
$ yarn run start


```



## Tecnologías Utilizadas

- [React Js](https://es.reactjs.org/) (Javascript)


## Prueba en vivo

[SPA Buscador by Daniel Pérez](https://item-finder-spa.web.app/)

## Imagenes

![Listado de artículos](https://bitbucket.org/CreativeDPC/item-finder-spa/raw/e7e9f82aa429842e9ae45fa468f8874bf1ed8184/pictures/item-list.png)

![Detalle de artículo](https://bitbucket.org/CreativeDPC/item-finder-spa/raw/e7e9f82aa429842e9ae45fa468f8874bf1ed8184/pictures/item-detail.png)
