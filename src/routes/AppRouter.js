import React from 'react'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
  } from 'react-router-dom';
import { SearchView } from '../views/SearchView';
import { DetailView } from '../views/DetailView';
import { Header } from '../components/layout/Header';

export const AppRouter = () => {
    return (
        <Router>
            <div className="principal-container">
                <Header/>
                <div className="rout-container">
                    <div className="wrap-grid">
                        <Switch> 
                            <Route exact path="/item/:idItem" component={ DetailView } />
                            
                            <Route exact path="/" component={ SearchView } />
                            
                            <Redirect to="/" /> 
                        </Switch>
                    </div>
                </div>
            </div>
        </Router>                

    )
}
