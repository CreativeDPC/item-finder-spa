import React, { useContext } from "react";
import { Path } from "../components/common/Path";
import { ItemsContext } from "../components/hooks/ItemsContext";
import { ItemList } from "../components/layout/ItemList";

export const SearchView = () => {
    const { resultSearch:{categories} } = useContext(ItemsContext);
    return (
        <>
            <Path paths={categories} />
            <div className="search-container grid-child">
                <ItemList />
            </div>
        </>
    )
}
