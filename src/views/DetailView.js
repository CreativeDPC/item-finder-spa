import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { Path } from "../components/common/Path";
import { Item } from "../logic/models/item.model";
import itemServices from "./../services/item.services";
import Parser from "./../utils/parser.util";

export const DetailView = () => {
    const [item, SetItem] = useState(new Item())
    const params = useParams();
    const {price, paths, picture, description, condition, sold_quantity, title} = item;

    useEffect(() =>{
        async function obtenerData(){
            if(params.idItem){
                const cloudResp = await itemServices.getDetailItemById(params.idItem);
                SetItem(cloudResp.item);
            }        
        }
        obtenerData();
    },[]);

    return (
        <>
            <Path paths={paths} />
            <div className="detail-container grid-child">
                <div id="info-item-region">
                    <img src={picture} alt="picture" />
                    <div id="description-item-region">
                        <h1>Descripción del Producto</h1>
                        <p>{description}</p>
                    </div>
                </div>
                <div id="features-item-region">
                    <span>
                        {`${condition === 'new' ? 'Nuevo' : condition} - ${sold_quantity} vendidos`}                         
                    </span>
                    <h3>{title}</h3>
                    <h1>{Parser.formatCurrency(price?.currency, price?.decimals, price?.amount)}</h1>
                    <button>Comprar</button>
                </div>
                
            </div>
        </>
    )
}
