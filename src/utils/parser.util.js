const formatCurrency = (currency, fractionDigits, number) => {
    var formatted = new Intl.NumberFormat("es-MX", {
      style: 'currency',
      currency: currency || 'MXN',
      useGrouping:true,
      minimumFractionDigits: fractionDigits || 2
    }).format(number || 0);
    return formatted;
  }

export default {
    formatCurrency
}