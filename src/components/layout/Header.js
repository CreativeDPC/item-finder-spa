import React from 'react'
import logo  from "./../../assets/images/Logo_ML.png";
import { Browser } from '../common/Browser';

export const Header = () => {

    return (
      <div className="header-container wrap-grid">
        <div id="logo-principal">
            <img src={logo} alt="logo" />
        </div>

        <div id="browser">
            <Browser />            
        </div>
      </div>
    );
}