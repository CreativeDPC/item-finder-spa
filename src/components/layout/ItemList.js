import React, { useContext } from 'react'
import { Item } from '../common/Item';
import { ItemsContext } from '../hooks/ItemsContext';

export const ItemList = () => {
    const { resultSearch:{author, items}, isFirstVisit } = useContext(ItemsContext);

    return (
        <>
            {
                (!items || items?.length === 0) && <h1 className="message-info">
                    { isFirstVisit ?
                    `Realiza una búsqueda para comenzar` :
                    `Ups! creo que no encontramos resultados para la búsqueda realizada.`}
                    
                </h1>
            }
            <ul className="item-list-container">
                {
                    items && 
                    items.map((item, i) =>{
                        return <li key={i}>
                                    <Item item={item} author={author}/>
                                </li>
                    })
                }
            </ul>
        </>
    );
}