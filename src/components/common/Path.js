import React from "react";

export const Path = ({paths}) => {
    
    return (
        <div className="path-container grid-child" >
            {
                paths?.length > 0 &&
                paths.map(path =>{
                    return <span>{path}</span>
                })
            }
        </div>
    )
}
