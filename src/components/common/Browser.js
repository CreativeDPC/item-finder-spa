import React, { useContext, useState } from 'react'
import { Redirect } from 'react-router-dom';
import { ItemsContext } from '../hooks/ItemsContext';
import iconSearch from "./../../assets/images/ic_Search.png";
import itemServices from "./../../services/item.services";

export const Browser = () => {
    const { setResultSearch, setIsFirstVisit, isFirstVisit } = useContext(ItemsContext);
    const [searchText, setSearchText] = useState("");
    const [isNewSearch, setIsNewSearch] = useState(false);

    const handleChange = (text) =>{
        setSearchText(text);
        setIsNewSearch(true)
    }

    const handleClickSearch = async () =>{
        return await getItems();
    }

    const handleKeyPress = async (code) =>{
        if(code === "Enter"){
          return await getItems();  
        }
    }

    const getItems = async () =>{
        const cloudResp = await itemServices.getItemsBySearchText(searchText);   
        setResultSearch(cloudResp);
        setSearchText("");
        setIsNewSearch(false);
        setIsFirstVisit(false) ;
    }

    return (
        <>
            <input type="text" placeholder="Nunca dejes de buscar" value={searchText} onChange={(e) => handleChange(e.target.value)} onKeyPress={(e) => handleKeyPress(e.key)}/>
            <button id="icon-search-wrap" onClick={handleClickSearch}>
                <img src={iconSearch} alt="icon-search" />
            </button>
            {
               !isNewSearch && !isFirstVisit && <Redirect to="/" />
            }
        </>
    );
}