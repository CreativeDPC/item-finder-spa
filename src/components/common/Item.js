import React from "react";
import shipping from './../../assets/images/ic_shipping.png';
import { Link } from 'react-router-dom';
import Parser from "./../../utils/parser.util";

export const Item = ({item, author}) => {
    const {id,title, picture, price, free_shipping} = item;
    const {name} = author;
    
    return (
        <div className="item-container" >
            <Link to={ `./item/${ id }` }>
                <img src={picture} alt="producto"/>
            </Link>
            <div id="info-search">
                <h3>
                    <Link to={ `./item/${ id }` }>
                        {Parser.formatCurrency(price?.currency, price?.decimals, price?.amount)}
                    </Link>
                    {
                        free_shipping && <img src={shipping} alt="shipping"/>
                    }
                    <span>{name || "CDMX"}</span>                    
                </h3>   
                <p>{title}</p>
            </div>            
        </div>
    )
}
