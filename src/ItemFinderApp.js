import { useState } from "react";
import { ItemsContext } from "./components/hooks/ItemsContext";
import { AppRouter } from "./routes/AppRouter";

function ItemFinderApp() {

  const [ resultSearch, setResultSearch ] = useState({});
  const [ isFirstVisit, setIsFirstVisit ] = useState(true);

  return (
    <ItemsContext.Provider value={{ resultSearch, setResultSearch , isFirstVisit, setIsFirstVisit}}>
      <AppRouter/>
    </ItemsContext.Provider>
  );
}

export default ItemFinderApp;
