import { Price } from "./price.model";

export class Item {
    constructor(data = {}){
        this.id = data.id || null;
        this.title = data.title || "";
        this.price = data.price || new Price();
        this.picture = data.picture || "";
        this.condition = data.condition || "";
        this.free_shipping = data.free_shipping || false;
        this.sold_quantity = data.sold_quantity;
        this.description = data.description;
        this.paths = data.paths || [];
    }

}

