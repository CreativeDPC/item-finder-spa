export class Price {
    constructor(data = {}){
        this.currency = data.currency || "";
        this.amount = data.amount || null;
        this.decimals = data.decimals || null;
    }
}