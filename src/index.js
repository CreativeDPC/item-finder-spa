import React from 'react';
import ReactDOM from 'react-dom';
import ItemFinderApp from './ItemFinderApp';
import './styles/styles.scss'

ReactDOM.render(
    <ItemFinderApp />,
  document.getElementById('root')
);

