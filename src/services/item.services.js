const urlBase = process.env.REACT_APP_URL_ITEM_SERVICE;

const getItemsBySearchText = async (text) =>{
    
    try {
        
        const resp = await fetch( `${urlBase}/items?q=${text}`, {
            method: 'GET'
        });

        if ( resp.ok ) {
            const cloudResp = await resp.json();
            return cloudResp;
        } else {
            throw await resp.json();
        }

    } catch (err) {
        throw err;
    }
}

const getDetailItemById = async (id) =>{
    
    try {
        
        const resp = await fetch( `${urlBase}/items/${id}`, {
            method: 'GET'
        });

        if ( resp.ok ) {
            const cloudResp = await resp.json();
            return cloudResp;
        } else {
            throw await resp.json();
        }

    } catch (err) {
        throw err;
    }
}

export default {
    getItemsBySearchText,
    getDetailItemById
}